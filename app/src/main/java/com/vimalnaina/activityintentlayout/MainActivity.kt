package com.vimalnaina.activityintentlayout

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnLinear = findViewById<Button>(R.id.btnLinear)
        val btnRelative = findViewById<Button>(R.id.btnRelative)
        val btnCoordinator = findViewById<Button>(R.id.btnCoordinator)

        btnLinear.setOnClickListener(){
            intent = Intent(applicationContext, LinearLayoutActivity::class.java)
            startActivity(intent)
        }

        btnRelative.setOnClickListener(){
            Intent(this,RelativeLayoutActivity::class.java).also {
                startActivity(it)
            }
        }

        btnCoordinator.setOnClickListener(){
            intent = Intent(applicationContext,CoordinatorLayoutActivity::class.java)
            startActivity(intent)
        }
    }
}